<?php

namespace Slts\Upload\Exceptions;

use Exception;

class NamingGenerationFailedException extends Exception
{
}
