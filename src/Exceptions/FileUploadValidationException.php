<?php

namespace Slts\Upload\Exceptions;

use Exception;

class FileUploadValidationException extends Exception
{
}
