<?php

namespace Slts\Upload\Exceptions;

use Exception;

class FileUploadFailureException extends Exception
{
}
