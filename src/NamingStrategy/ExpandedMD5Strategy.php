<?php

namespace Slts\Upload\NamingStrategy;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\NamingGenerationFailedException;
use Slts\Upload\NamingStrategyInterface;
use Symfony\Component\Mime\MimeTypes;

class ExpandedMD5Strategy implements NamingStrategyInterface
{
    private $retries;
    private $mimeTypes;

    public function __construct(int $retries = 10)
    {
        $this->retries = $retries;
        $this->mimeTypes = new MimeTypes();
    }

    public function generate(FileUpload $fileUpload, array $context = []): string
    {
        $filesystem = $context['filesystem'] ?? null;
        $uploadPathPrefix = $context['uploadPathPrefix'] ?? '';
        if (null === $filesystem) {
            throw new NamingGenerationFailedException('Missing "filesystem" context');
        }

        for ($i = 0; $i < $this->retries; $i++) {
            $hash = md5(uniqid(mt_rand(), true));
            $path = $this->expandPath($hash . $this->guessExtension($fileUpload), $uploadPathPrefix);
            if (!$filesystem->has($path)) {
                return $path;
            }
        }
        throw new NamingGenerationFailedException("Failed even after {$this->retries} retries");
    }

    private function guessExtension(FileUpload $fileUpload): string
    {
        $mimeType =  $this->mimeTypes->getMimeTypes($fileUpload->getContentType())[0] ?? '';
        $extension = $this->mimeTypes->getExtensions($mimeType)[0] ?? null;
        if (null !== $extension) {
            return '.' . $extension;
        }

        return '';
    }

    private function expandPath(string $path, $uploadPathPrefix = '')
    {
        $pattern = '%s/%s/%s';
        $args = [
            substr($path, 0, 2),
            substr($path, 2, 2),
            substr($path, 4),
        ];
        if ($uploadPathPrefix) {
            $pattern .= '%s';
            array_unshift($args, $uploadPathPrefix);
        }
        return sprintf(
            $pattern,
            ...$args
        );
    }
}
