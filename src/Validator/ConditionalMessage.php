<?php

namespace Slts\Upload\Validator;

class ConditionalMessage
{
    private $message;
    private $parameters;

    public function __construct(string $message, array $parameters)
    {
        $this->message = $message;
        $this->parameters = $parameters;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }
}
