<?php

namespace Slts\Upload\Validator;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadValidationException;

class FileTypeValidator implements FileUploadValidatorInterface
{
    private $mimeTypes;

    public function __construct(array $mimeTypes)
    {
        $this->mimeTypes = $mimeTypes;
    }

    public function validate(FileUpload $file): void
    {
        $mt = $file->getContentType();
        foreach ($this->mimeTypes as $mimeType) {
            if ($mimeType === $mt) {
                return;
            }
        }

        throw new FileUploadValidationException('File has invalid mimetype');
    }

    public function getConditionMessage()
    {
        $types = implode(', ', $this->mimeTypes);
        return new ConditionalMessage('async-uploader.validators.file-type', ['types' => $types]);
    }

    public function toDropzoneString()
    {
        return implode(',', $this->mimeTypes);
    }
}
