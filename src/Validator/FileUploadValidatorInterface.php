<?php

namespace Slts\Upload\Validator;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadValidationException;

interface FileUploadValidatorInterface
{
    /**
     * @param FileUpload $file
     * @throws FileUploadValidationException
     */
    public function validate(FileUpload $file): void;

    public function getConditionMessage();
}
