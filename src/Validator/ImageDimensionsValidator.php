<?php

namespace Slts\Upload\Validator;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadValidationException;

class ImageDimensionsValidator implements FileUploadValidatorInterface
{
    private $minWidth;

    private $minHeight;

    private $maxWidth;

    private $maxHeight;

    public function __construct(int $minWidth = null, int $minHeight = null, int $maxWidth = null, int $maxHeight = null)
    {
        $this->minWidth = $minWidth;
        $this->minHeight = $minHeight;
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
    }

    public static function createMin(int $minWidth, int $minHeight)
    {
        return new static($minWidth, $minHeight);
    }

    public static function createMax(int $maxWidth, int $maxHeight)
    {
        return new static($maxWidth, $maxHeight);
    }

    /**
     * @param FileUpload $file
     *
     * @return void
     *
     * @throws FileUploadValidationException
     */
    public function validate(FileUpload $file): void
    {
        $path = $file->getTemporaryFile();
        $mimeType = $file->getContentType();
        if (!$mimeType || !in_array($mimeType, ['image/jpeg', 'image/png', 'image/gif'])) {
            return;
        }
        
        $dimensions = @getimagesize($path); // @see \Nette\Http\FileUpload::getImageSize
        if (!$dimensions || !$this->isValid($dimensions[0], $dimensions[1])) {
            throw new FileUploadValidationException('Obrázek má nedostatečné rozměry');
        }
    }

    private function isValid($width, $height): bool
    {
        if (null !== $this->minWidth && $width < $this->minWidth) {
            return false;
        }
        if (null !== $this->minHeight && $height < $this->minHeight) {
            return false;
        }
        if (null !== $this->maxWidth && $width > $this->maxWidth) {
            return false;
        }
        if (null !== $this->maxHeight && $height > $this->maxHeight) {
            return false;
        }
        return true;
    }

    public function getConditionMessage()
    {
        if (null !== $this->minWidth && null !== $this->minHeight) {
            $params = ['width' => $this->minWidth, 'height' => $this->minHeight];
            return new ConditionalMessage('async-uploader.validators.image-dimensions-min-w-h', $params);
        }
        return new ConditionalMessage('', []);
    }
}
