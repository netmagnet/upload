<?php

namespace Slts\Upload\Validator;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadValidationException;

class FileUploadOkValidator implements FileUploadValidatorInterface
{
    public function __construct()
    {
    }

    public function validate(FileUpload $file): void
    {
        if (!$file->isOk()) {
            throw new FileUploadValidationException('File too big');
        }
    }

    public function getConditionMessage()
    {
        $number = ini_get('upload_max_filesize');
        return new ConditionalMessage('async-uploader.validators.file-ok', ['number' => $number]);
    }
}
