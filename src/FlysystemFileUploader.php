<?php

namespace Slts\Upload;

use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadFailureException;
use Slts\Upload\Exceptions\FileUploadValidationException;
use Slts\Upload\Exceptions\NamingGenerationFailedException;

class FlysystemFileUploader implements FileUploaderInterface
{
    protected $filesystem;
    protected $namingStrategy;

    public function __construct(
        FilesystemInterface $filesystem,
        NamingStrategyInterface $namingStrategy
    ) {
        $this->filesystem = $filesystem;
        $this->namingStrategy = $namingStrategy;
    }

    public function upload(FileUpload $fileUpload, array $validators)
    {
        $this->validate($fileUpload, $validators);

        return $this->doUpload($fileUpload);
    }

    public function validate(FileUpload $fileUpload, array $validators)
    {
        foreach ($validators as $validator) {
            try {
                $validator->validate($fileUpload);
            } catch (FileUploadValidationException $e) {
                throw new FileUploadFailureException('File upload failed', 0, $e);
            }
        }
    }

    protected function doUpload(FileUpload $fileUpload)
    {
        $path = $this->generatePath($fileUpload);
        try {
            $file = $this->filesystem->moveNetteFileUpload($path, $fileUpload);
        } catch (FileExistsException $e) {
            throw new FileUploadFailureException('', 0, $e);
        }

        if (false === $file) {
            throw new FileUploadFailureException('Error TODO');
        }

        return $file;
    }

    protected function generatePath(FileUpload $fileUpload): string
    {
        try {
            return $this->namingStrategy->generate($fileUpload, ['filesystem' => $this->filesystem]);
        } catch (NamingGenerationFailedException $e) {
            throw new FileUploadFailureException('', 0, $e);
        }
    }
}
