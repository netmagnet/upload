<?php

namespace Slts\Upload;

use League\Flysystem\FilesystemInterface;
use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadFailureException;
use Slts\Upload\Exceptions\NamingGenerationFailedException;

class PrefixedFlysystemFileUploader extends FlysystemFileUploader
{
    protected $uploadPathPrefix;

    public function __construct(
        string $uploadPathPrefix,
        FilesystemInterface $filesystem,
        NamingStrategyInterface $namingStrategy
    ) {
        parent::__construct($filesystem, $namingStrategy);
        $this->uploadPathPrefix = $uploadPathPrefix;
    }

    protected function generatePath(FileUpload $fileUpload): string
    {
        try {
            return $this->namingStrategy->generate(
                $fileUpload,
                ['filesystem' => $this->filesystem, 'uploadPathPrefix' => $this->uploadPathPrefix]
            );
        } catch (NamingGenerationFailedException $e) {
            throw new FileUploadFailureException('', 0, $e);
        }
    }
}
