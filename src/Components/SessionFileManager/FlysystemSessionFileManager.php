<?php

namespace Slts\Upload\Components\SessionFileManager;

use League\Flysystem\FilesystemInterface;
use Nette\Http\Session;

class FlysystemSessionFileManager extends SessionFileManager
{
    protected $filesystem;

    public function __construct(
        string $tokenKeyPrefix,
        ?int $minFiles,
        ?int $maxFiles,
        Session $session,
        FilesystemInterface $filesystem
    ) {
        parent::__construct($tokenKeyPrefix, $minFiles, $maxFiles, $session);
        $this->filesystem = $filesystem;
    }

    public function getFiles()
    {
        $files = parent::getFiles();
        return array_map(function ($path) {
            return $this->filesystem->get($path);
        }, $files);
    }

    public function addFile($file)
    {
        parent::addFile($file->getPath());
    }
}
