<?php

namespace Slts\Upload\Components\SessionFileManager;

interface SessionFileManagerFactoryInterface
{
    public function create(string $tokenKeyPrefix, ?int $minFiles, ?int $maxFiles): SessionFileManager;
}
