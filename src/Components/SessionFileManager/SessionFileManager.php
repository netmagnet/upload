<?php

namespace Slts\Upload\Components\SessionFileManager;

use Nette\Application\UI\Control;
use Nette\Forms\Form;
use Nette\Http\Session;
use Nette\Utils\Random;
use Slts\Upload\FileManagerInterface;

class SessionFileManager extends Control implements FileManagerInterface
{
    protected $tokenKey;
    protected $session;
    protected $token;
    protected $tokenKeyPrefix;
    protected $filesystem;
    protected $valid;
    protected $minFiles;
    protected $maxFiles;

    public function __construct(string $tokenKeyPrefix, ?int $minFiles, ?int $maxFiles, Session $session)
    {
        $this->session = $session;
        $this->session->start();
        $this->tokenKeyPrefix = $tokenKeyPrefix;
        $this->tokenKey = $tokenKeyPrefix . '-manager';
        $this->onAnchor[] = function () {
            $this->token = (string)$this->loadToken();
        };
        $this->validateLimits($minFiles, $maxFiles);
    }

    protected function validateLimits(?int $minFiles, ?int $maxFiles)
    {
        if (null !== $minFiles && $minFiles < 0) {
            throw new \InvalidArgumentException('Argument minFiles must be higher than 0');
        }
        if (null !== $maxFiles && ($maxFiles < 1 || $maxFiles < $minFiles)) {
            throw new \InvalidArgumentException('Argument maxFiles must be higher than 1 or higher than minFiles');
        }
        $this->minFiles = $minFiles;
        $this->maxFiles = $maxFiles;
    }

    protected function loadToken()
    {
        $request = $this->getPresenter()->getHttpRequest();

        return $request->getPost($this->tokenKey) ?? $this->getParameter('token') ?? Random::generate();
    }

    protected function getSection()
    {
        return $this->session->getSection('FileUpload');
    }

    public function getFiles()
    {
        $section = $this->getSection();
        $files = $section->offsetGet($this->token);
        if (null === $files) {
            return [];
        }

        return $files;
    }

    public function addFile($file)
    {
        $section = $this->getSection();
        $arr = $section->offsetGet($this->token);
        $section->setExpiration('+ 1 day', $this->token);
        $arr[] = $file;
        $section->offsetSet($this->token, $arr);
        $this->redrawControl('file-list');
    }

    public function clean()
    {
        $section = $this->getSection();
        $section->offsetUnset($this->token);
    }

    public function isValid()
    {
        $files = $this->getFiles();
        $count = \count($files);
        if ($this->minFiles && $this->minFiles > $count) {
            return false;
        }
        if ($this->maxFiles && $this->maxFiles < $count) {
            return false;
        }
        return true;
    }

    public function render()
    {
        $files = $this->getFiles();

        /** @var Form|null $form */
        $form = $this->lookup(Form::class, false);
        $isSubmitted = $form && $form->isSubmitted();

        $this->template->tokenKeyPrefix = $this->tokenKeyPrefix;
        $this->template->tokenKey = $this->tokenKey;
        $this->template->files = $files;
        $this->template->token = $this->token;
        $this->template->displayError = $isSubmitted && false === $this->isValid();
        $this->template->setFile($this->getTemplatePath());
        $this->template->render();
    }

    protected function getTemplatePath()
    {
        return __DIR__ . '/templates/sessionFileManager.latte';
    }

    public function renderJavascript()
    {
        $this->template->render(
            $this->getJavascriptTemplatePath(),
            [
                'tokenKeyPrefix' => $this->tokenKeyPrefix,
                'tokenKey' => $this->tokenKey,
                'token' => $this->token,
            ]
        );
    }

    protected function getJavascriptTemplatePath()
    {
        return __DIR__ . '/templates/script.sessionFileManager.latte';
    }

    public function handleRefresh()
    {
        $this->redrawControl('file-list');
    }
}
