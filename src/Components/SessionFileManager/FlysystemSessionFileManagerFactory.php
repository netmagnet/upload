<?php

namespace Slts\Upload\Components\SessionFileManager;

use League\Flysystem\FilesystemInterface;
use Nette\Http\Session;

class FlysystemSessionFileManagerFactory
{
    private $filesystem;
    private $session;

    public function __construct(
        FilesystemInterface $filesystem,
        Session $session
    ) {
        $this->filesystem = $filesystem;
        $this->session = $session;
    }

    public function create(string $tokenKeyPrefix, ?int $minFiles, ?int $maxFiles): FlysystemSessionFileManager
    {
        return new FlysystemSessionFileManager($tokenKeyPrefix, $minFiles, $maxFiles, $this->session, $this->filesystem);
    }
}
