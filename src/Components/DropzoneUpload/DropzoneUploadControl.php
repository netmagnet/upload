<?php

namespace Slts\Upload\Components\DropzoneUpload;

use Nette\Http\FileUpload;
use Nette\Utils\Html;
use Nextras\FormComponents\Fragments\UIControl\BaseControl;
use Slts\Upload\Exceptions\FileUploadFailureException;
use Slts\Upload\FileManagerFactoryInterface;
use Slts\Upload\FileManagerInterface;
use Slts\Upload\FileUploaderInterface;
use Slts\Upload\Validator\FileTypeValidator;
use Slts\Upload\Validator\FileUploadOkValidator;

class DropzoneUploadControl extends BaseControl
{
    protected $fileManagerFactory;
    protected $fileUploader;
    protected $validators;
    protected $acceptedFiles;

    public function __construct(
        string $caption = null,
        FileManagerFactoryInterface $fileManagerFactory,
        FileUploaderInterface $fileUploader,
        array $validators = []
    ){
        parent::__construct($caption);
        $this->fileManagerFactory = $fileManagerFactory;
        $this->fileUploader = $fileUploader;
        $this->setupValidators($validators);
        $this->acceptedFiles = $this->prepareAcceptedFiles($validators);
    }

    protected function setupValidators(array $validators)
    {
        array_unshift($validators, new FileUploadOkValidator());
        $this->validators = $validators;
    }

    private function prepareAcceptedFiles(array $acceptedFiles)
    {
        $acceptedFiles = array_map(function ($rule) {
            if (!$rule instanceof FileTypeValidator) {
                return false;
            }
            return $rule->toDropzoneString();
        }, $acceptedFiles);
        $acceptedFiles = array_filter($acceptedFiles, function ($rule) {
            return $rule;
        });

        return implode(',', $acceptedFiles);
    }

    /* Component control stuff */

    public function handleUpload()
    {
        $fileUploads = $this->getPresenter()->getHttpRequest()->getFiles()['file'] ?? null;
        if (null === $fileUploads) {
            $this->getPresenter()->getHttpResponse()->setCode(400);
            $this->getPresenter()->sendJson(['error' => 'Upload failed']);
        }

        $singleFileUpload = \is_array($fileUploads) ? array_shift($fileUploads) : $fileUploads;
        if (! $singleFileUpload instanceof FileUpload) {
            $this->getPresenter()->getHttpResponse()->setCode(400);
            $this->getPresenter()->sendJson(['error' => 'Upload failed']);
        }

        try {
            $uploaded = $this->fileUploader->upload($singleFileUpload, $this->validators);
            $this['manager']->addFile($uploaded);
        } catch (FileUploadFailureException $e) {
            $this->getPresenter()->getHttpResponse()->setCode(400);
            $msg = $e->getPrevious() ? $e->getPrevious()->getMessage() : $e->getMessage();
            $this->getPresenter()->sendJson(['error' => $this->translate($msg)]);
        }

        $response = '';

        $this->getPresenter()->getHttpResponse()->setCode(200);
        $this->getPresenter()->sendJson($response);
    }

    public function render()
    {
        $template = $this->getTemplate();
        $template->htmlId = $this->getHtmlId();
        $template->uploadInputPath = $this->getUploadInputTemplatePath();
        $template->inputName = $this->getName();

        $template->acceptedFiles = $this->acceptedFiles;
        $template->errors = $this->getErrors();
        $template->validators = $this->validators;

        $template->setFile($this->getTemplatePath());
        $template->render();
    }

    protected function createComponentManager(): FileManagerInterface
    {
        return $this->fileManagerFactory->create($this->getHtmlId());
    }

    /* Form control stuff */

    public function setValue($value)
    {
        // yes, do nothing
    }

    public function getValue()
    {
        return $this['manager']->getFiles();
    }

    public function validate(array $controls = null): void
    {
        /** @var FileManagerInterface $manager */
        $manager = $this['manager'];
        if (!$manager->isValid()) {
            $this->addError('async-uploader.missing-files');
        }
    }

    public function getControl()
    {
        ob_start();
        $this->render();
        
        return Html::el()->addHtml(ob_get_clean());
    }
    
    protected function getTemplatePath()
    {
        return __DIR__ . '/templates/uploadControl.latte';
    }

    protected function getUploadInputTemplatePath()
    {
        return __DIR__ . '/templates/uploadInput.latte';
    }
}
