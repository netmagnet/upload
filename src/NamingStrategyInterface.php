<?php

namespace Slts\Upload;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\NamingGenerationFailedException;

interface NamingStrategyInterface
{
    /**
     * @param FileUpload $fileUpload
     * @param array      $context
     *
     * @return string
     * @throws NamingGenerationFailedException
     */
    public function generate(FileUpload $fileUpload, array $context = []): string;
}
