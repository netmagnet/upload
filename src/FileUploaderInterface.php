<?php

namespace Slts\Upload;

use Nette\Http\FileUpload;
use Slts\Upload\Exceptions\FileUploadFailureException;

interface FileUploaderInterface
{
    /**
     * @param FileUpload $fileUpload
     * @param array      $validators
     *
     * @return mixed
     * @throws FileUploadFailureException
     */
    public function upload(FileUpload $fileUpload, array $validators);
}
