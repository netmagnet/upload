<?php

namespace Slts\Upload;

interface FileManagerInterface
{
    public function getFiles();

    public function addFile($file);

    public function isValid();

    public function render();
    
    public function renderJavascript();

    public function clean();
}
