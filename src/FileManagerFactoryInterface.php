<?php

namespace Slts\Upload;

interface FileManagerFactoryInterface
{
    public function create(string $tokenKeyPrefix): FileManagerInterface;
}
